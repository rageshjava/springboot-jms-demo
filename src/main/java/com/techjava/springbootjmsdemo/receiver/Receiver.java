package com.techjava.springbootjmsdemo.receiver;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;

import java.util.concurrent.CountDownLatch;

public class Receiver {

    private static final Logger LOG = LoggerFactory.getLogger(Receiver.class);

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    @JmsListener(destination = "${activemq.msg.destination}")
    public void receive(String message){
        LOG.info("Receiveing message = '{}'", message);
        countDownLatch.countDown();
    }
}
