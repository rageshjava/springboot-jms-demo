package com.techjava.springbootjmsdemo;

import com.techjava.springbootjmsdemo.receiver.Receiver;
import com.techjava.springbootjmsdemo.sender.Sender;
import org.apache.activemq.junit.EmbeddedActiveMQBroker;
import org.junit.AfterClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringJmsDemoTest {

    private static ApplicationContext applicationContext;

    @Autowired
    void setContext(ApplicationContext applicationContext){
        SpringJmsDemoTest.applicationContext = applicationContext;
    }

    @AfterClass
    public static void afterClass(){
        ((ConfigurableApplicationContext)applicationContext).close();
    }

    @ClassRule
    public static EmbeddedActiveMQBroker embeddedActiveMQBroker = new EmbeddedActiveMQBroker();

    @Autowired
    private Sender sender;

    @Autowired
    private Receiver receiver;

    @Test
    public void testReceiver()throws Exception{
        sender.send("hellojava.q", "Welcome to Springboot JMS Demo");
        receiver.getCountDownLatch().await(10000, TimeUnit.MILLISECONDS);
        assertThat(receiver.getCountDownLatch().getCount()).isEqualTo(0);
    }

}
