package com.techjava.springbootjmsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJmsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJmsDemoApplication.class, args);
	}
}
