package com.techjava.springbootjmsdemo.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

public class Sender {

    private static final Logger LOG = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public void send(String destination, String message){
        LOG.info("sending destination = {} and message = '{}'", destination, message);
        jmsTemplate.convertAndSend(destination, message);
    }
}
